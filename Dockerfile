FROM centos:7
LABEL maintainer="DORIC"

#指明该镜像的作者和其电子邮件（这里随便就好）
MAINTAINER wangnian "1835334431@qq.com"

#------------------- 安装JAVA8 -------------------
# ADD命令自动解压缩
ADD ./jdk-8u301-linux-x64.tar.gz /usr/local
# 切换到镜像中的指定路径，设置工作目录
WORKDIR /usr/local
RUN mv jdk1.8.0_301 /usr/local/java
# 设置JAVA环境变量
ENV JAVA_HOME /usr/local/java
ENV JAVA_BIN /usr/local/java/bin
ENV JRE_HOME /usr/local/java/jre
ENV PATH $PATH:/usr/local/java/bin:/usr/local/java/jre/bin
ENV CLASS_PATH /usr/local/java/jre/bin:/usr/local/java/lib:/usr/local/java/jre/lib/charsets.jar

#------------------- 安装编译所需要的工具及库 -------------------
RUN yum -y install  \
bzip2 \ 
gcc \
make \
subversion \
gcc-c++ \
sqlite-devel \
libxml2-devel \
python-devel \
numpy \
swig \
expat-devel \
libcurl-devel \ 
postgresql \ 
postgresql-devel

#------------------- 安装geos -------------------
COPY ./geos-3.6.2.tar.bz2 /usr/local/
WORKDIR /usr/local
RUN tar -xvf geos-3.6.2.tar.bz2
WORKDIR /usr/local/geos-3.6.2
RUN ./configure
RUN make && make install
RUN rm -rf /usr/local/geos-3.6.2.tar.bz2
RUN rm -rf /usr/local/geos-3.6.2

#------------------- 安装proj4 -------------------
COPY ./proj-4.9.3.tar.gz /usr/local/
WORKDIR /usr/local
RUN tar -xvf proj-4.9.3.tar.gz
WORKDIR /usr/local/proj-4.9.3
RUN ./configure
RUN make && make install
RUN rm -rf /usr/local/proj-4.9.3.tar.gz
RUN rm -rf /usr/local/proj-4.9.3

#------------------- 安装libxml2 -------------------
RUN yum install -y libxml2
RUN yum install -y libxml2-devel

# ------------------- 安装filegdb api -------------------
COPY ./FileGDB_API_1_5_64.tar.gz /usr/local/
WORKDIR /usr/local
RUN gunzip FileGDB_API_1_5_64.tar.gz
RUN tar -xvf FileGDB_API_1_5_64.tar
RUN mv FileGDB_API-64 /usr/local/FileGDB_API
# 必须先暴露动态库，否则报未定义参考错误
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/FileGDB_API/lib
RUN echo $LD_LIBRARY_PATH

WORKDIR /usr/local/FileGDB_API/samples
RUN make
RUN cp ../lib/* /usr/local/lib/
RUN rm -rf /usr/local/FileGDB_API_1_5_64.tar
RUN rm -rf /usr/local/FileGDB_API_1_5_64.tar.gz

# https://gis.stackexchange.com/questions/292506/how-do-i-install-esri-file-gdb-api-in-ubuntu-16-04-so-qgis-2-8-can-see-it
# cd src/FileGDB_API-64gcc51
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/lib
# cd samples
# make
# cp ../lib/* /usr/local/lib/
# ldconfig


#------------------- 安装ant -------------------
# 为了编译gdal.jar
COPY apache-ant-1.10.7-bin.tar.gz /usr/local
WORKDIR /usr/local
RUN tar -xvf apache-ant-1.10.7-bin.tar.gz
RUN mv apache-ant-1.10.7 ant
RUN rm -rf /usr/local/apache-ant-1.10.7-bin.tar.gz

#-------------------  安装swig ------------------- 
COPY swig-4.0.1.tar.gz /usr/local
WORKDIR /usr/local
RUN tar -xvf swig-4.0.1.tar.gz
WORKDIR /usr/local/swig-4.0.1
RUN ./configure \
--prefix=/usr/local/swig \ 
--without-pcre
RUN make && make install

RUN rm -rf /usr/local/swig-4.0.1
RUN rm -rf /usr/local/swig-4.0.1.tar.gz

ENV ANT_HOME /usr/local/ant
ENV SWIG_HOME /usr/local/swig
ENV PATH $PATH:$ANT_HOME/bin:$SWIG_HOME/bin
RUN ldconfig

#------------------- 安装GDAL -------------------
# 解压缩gdal到/usr/local
COPY gdal-2.4.4.tar.gz /usr/local
WORKDIR /usr/local
RUN tar -xvf gdal-2.4.4.tar.gz
WORKDIR /usr/local/gdal-2.4.4
RUN ./configure \
--prefix=/usr/local/gdal \
--with-pg \ 
--with-java=/usr/local/java \ 
--with-curl=/usr/bin/curl-config \ 
--with-xml2=/usr/bin/xml2-config \ 
--with-fgdb=/usr/local/FileGDB_API
RUN make && make install

# 配置GDAL环境变量
ENV GDAL_HOME /usr/local/gdal
ENV GDAL_DATA $GDAL_HOME/share/gdal
RUN ldconfig

WORKDIR /usr/local/gdal
RUN cp -r ./bin/* /usr/local/bin/
RUN cp -r ./lib/* /usr/local/lib/

# 可执行文件和动态链接库添加到环境变量中
ENV PATH $PATH:/usr/local/bin
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/lib
RUN ldconfig

#------------------- 编译gdal.jar -------------------
WORKDIR /usr/local/gdal-2.4.4/swig/java
# 修改java.opt的java_home
RUN sed -i '5aJAVA_HOME = /usr/local/java' java.opt
RUN make && make install

# 复制so文件，否则报错，java.lang.UnsatisfiedLinkError:org.gdal.gdal.gdalJNI.AllRegister()V
RUN cp gdal.jar /usr/local/java/jre/lib/ext/
RUN cp *.so /usr/local/java/jre/lib/amd64/server/
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/java/jre/lib/amd64/server

# RUN rm -rf /usr/local/gdal-2.4.4
RUN rm -rf /usr/local/gdal-2.4.4.tar.gz

WORKDIR /usr/local









